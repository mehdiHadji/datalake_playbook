# Install Command
```sh
ansible-playbook -i hosts site.yml
``` 

# Extra configuration (After installation)
TODO: extra conf to be added on playbooks
## 1. Python
### 1.1 Install package with pip
Install a package with pip for a specific system Python:
```sh
python3.7 -m pip install pyspark==2.4.5
```
### 1.2 Fix distutils issue
Create distutils dir for python3.7
```sh
mkdir -p /usr/lib/python3.7/distutils
```

Copy distutils requirements into python3.7 
```sh
sudo cp -a /usr/lib/python3.8/distutils/* /usr/lib/python3.7/distutils/
```

## 2. Hadoop

### 2.1 Fix Java home on hadoop-env.sh

On ```${HADOOP_HOME}/etc/hadoop/hadoop-env.sh```, replace ${JAVA_HOME}:
```sh
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre"
```

### 2.2  Manage HDFS permissions when stopping / starting Namenode and Datanode

Connect to root:
```sh
sudo su
```

Command 1:
```sh
ssh-keygen
```

Command 2:
```sh
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
```

Test stopping / starting nodes, go to $HADOOP_HOME/sbin:
```sh
sudo ./stop-all.sh
```

```sh
sudo ./start-all.sh
```


## 2. Spark

### 2.1 Use a specific python
Open spark-env.sh and add * :
```sh
export PYSPARK_PYTHON=/usr/bin/python3.7
```

AND
```sh
export PYSPARK_DRIVER_PYTHON=/usr/bin/python3.7
```


