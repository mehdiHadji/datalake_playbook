#!/bin/bash


for db in 'dbespmobiledgi' 'dbradeej' 'dbespouest' 'dbespradeej' 'dbesprabat' 'dbamanpay' 'dbespsud' 'dbesptanger' 'dbgwclamf1' 'dbradem' 'dbrak' 'dbramsa' 'dbrech' 'dbredal' 'dbstareo' 'dbtax' 'dbiam' 'dbespagadir' 'dbbo' 'dbespgwmc' 'dbespmobileradees' 'dbespnord2' 'dbespouest2' 'dbespsud2' 'dbradeeta' 'dbtrxradeej' 'dbacciona' 'dbamantyweb' 'dbamendis' 'dbjawaz' 'dbcash' 'dblydec' 'dbctm' 'dbdgi' 'dbdhl' 'dbespcasa1' 'dbmobicash' 'dbmoneygram' 'dboncf' 'dborange' 'dbonep' 'dbespcasa2' 'dbradeef' 'dbespest' 'dbradeel' 'dbespest2' 'dbespgwcl1' 'dbespmobilebcp' 'dbespmobilecanalm' 'dbradeeo' 'dbespmobilecashplus' 'dbespnord' 'dbradees' 'dbradeema' 'dbsync' 'dbtrx'
do
      echo "Database : ${db}"
       psql -U postgres -d $db -c "DO
\$do\$
    DECLARE
        res RECORD;
    BEGIN
        FOR res IN (SELECT table_name
                    FROM information_schema.tables
                    WHERE table_schema = 'public'
                      AND table_type = 'BASE TABLE'
                      AND table_name  NOT IN (
                        select tc.table_name
                        from information_schema.table_constraints tc
                                 join information_schema.key_column_usage kc
                                      on kc.table_name = tc.table_name and kc.table_schema = tc.table_schema and
                                         kc.constraint_name = tc.constraint_name
                        where tc.constraint_type = 'PRIMARY KEY'
                          and kc.ordinal_position is not null
                        order by tc.table_schema,
                                 tc.table_name,
                                 kc.position_in_unique_constraint)) LOOP
EXECUTE  format('ALTER TABLE %s ADD COLUMN pk_id BIGSERIAL PRIMARY KEY', concat('public.',res.table_name)) ;
            END LOOP;
    END
\$do\$;"
done

echo $! > pid



