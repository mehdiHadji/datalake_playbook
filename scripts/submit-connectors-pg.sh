#!/bin/bash
dbhostname=${1:-'localhost'}
dbusername=${2:-'mbamouh'}
dbpassword=${3:-'xhub'}
dbport=${4:-'5432'}

for db in 'dbespmobiledgi' 'dbradeej' 'dbespouest' 'dbespradeej' 'dbesprabat' 'dbamanpay' 'dbespsud' 'dbesptanger' 'dbgwclamf1' 'dbradem' 'dbrak' 'dbramsa' 'dbrech' 'dbredal' 'dbstareo' 'dbtax' 'dbiam' 'dbespagadir' 'dbbo' 'dbespgwmc' 'dbespmobileradees' 'dbespnord2' 'dbespouest2' 'dbespsud2' 'dbradeeta' 'dbtrxradeej' 'dbacciona' 'dbamantyweb' 'dbamendis' 'dbjawaz' 'dbcash' 'dblydec' 'dbctm' 'dbdgi' 'dbdhl' 'dbespcasa1' 'dbmobicash' 'dbmoneygram' 'dboncf' 'dborange' 'dbonep' 'dbespcasa2' 'dbradeef' 'dbespest' 'dbradeel' 'dbespest2' 'dbespgwcl1' 'dbespmobilebcp' 'dbespmobilecanalm' 'dbradeeo' 'dbespmobilecashplus' 'dbespnord' 'dbradees' 'dbradeema' 'dbsync' 'dbtrx'
do
      curl -s -X POST -H 'Content-Type: application/json' --data "{
  \"name\": \"PostgreSQL ${db}\",
  \"config\": {
    \"connector.class\": \"io.debezium.connector.postgresql.PostgresConnector\",
    \"errors.log.include.messages\": \"true\",
    \"database.dbname\": \"${db}\",
    \"database.user\": \"${dbusername}\",
    \"database.server.name\": \"${db}\",
    \"plugin.name\": \"pgoutput\",
    \"database.port\": \"${dbport}\",
    \"schema.whitelist\": \"public\",
    \"value.converter.schema.registry.url\": \"http://schema-registry:8081\",
    \"database.hostname\": \"${dbhostname}\",
    \"database.password\": \"${dbpassword}\",
    \"value.converter\": \"org.apache.kafka.connect.json.JsonConverter\",
    \"value.converter.schemas.enable\": \"false\",
    \"errors.log.enable\": \"true\",
    \"key.converter\": \"org.apache.kafka.connect.json.JsonConverter\",
    \"key.converter.schemas.enable\": \"false\",
    \"key.converter.schema.registry.url\": \"http://schema-registry:8081\",
    \"slot.name\" : \"${db}\"
  }
}" http://localhost:8083/connectors

done

