#!/bin/bash

hostname=${1:-'localhost'}
port=${2:-'5432'}
dbuser=${3:-'mbamouh'}
dbpass=${4:-'xhub'}

for db in 'dbespmobiledgi' 'dbradeej' 'dbespouest' 'dbespradeej' 'dbesprabat' 'dbamanpay' 'dbespsud' 'dbesptanger' 'dbgwclamf1' 'dbradem' 'dbrak' 'dbramsa' 'dbrech' 'dbredal' 'dbstareo' 'dbtax' 'dbiam' 'dbespagadir' 'dbbo' 'dbespgwmc' 'dbespmobileradees' 'dbespnord2' 'dbespouest2' 'dbespsud2' 'dbradeeta' 'dbtrxradeej' 'dbacciona' 'dbamantyweb' 'dbamendis' 'dbjawaz' 'dbcash' 'dblydec' 'dbctm' 'dbdgi' 'dbdhl' 'dbespcasa1' 'dbmobicash' 'dbmoneygram' 'dboncf' 'dborange' 'dbonep' 'dbespcasa2' 'dbradeef' 'dbespest' 'dbradeel' 'dbespest2' 'dbespgwcl1' 'dbespmobilebcp' 'dbespmobilecanalm' 'dbradeeo' 'dbespmobilecashplus' 'dbespnord' 'dbradees' 'dbradeema' 'dbsync' 'dbtrx'
do
       sqoop import-all-tables --connect jdbc:postgresql://${hostname}:${port}/${db} --username ${dbuser} --password ${dbpass}  --as-avrodatafile --warehouse-dir /databases/${db} -- --default-character-set=utf8
done

echo $! > pid



