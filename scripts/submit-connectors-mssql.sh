#!/bin/bash
dbhostname=$1
dbusername=$2
dbpassword=$3
dbport=$4
kafkaserver=$5

for db in 'dbespmobiledgi' 'dbradeej' 'dbespouest' 'dbespradeej' 'dbesprabat' 'dbamanpay' 'dbespsud' 'dbesptanger' 'dbgwclamf1' 'dbradem' 'dbrak' 'dbramsa' 'dbrech' 'dbredal' 'dbstareo' 'dbtax' 'dbiam' 'dbespagadir' 'dbbo' 'dbespgwmc' 'dbespmobileradees' 'dbespnord2' 'dbespouest2' 'dbespsud2' 'dbradeeta' 'dbtrxradeej' 'dbacciona' 'dbamantyweb' 'dbamendis' 'dbjawaz' 'dbcash' 'dblydec' 'dbctm' 'dbdgi' 'dbdhl' 'dbespcasa1' 'dbmobicash' 'dbmoneygram' 'dboncf' 'dborange' 'dbonep' 'dbespcasa2' 'dbradeef' 'dbespest' 'dbradeel' 'dbespest2' 'dbespgwcl1' 'dbespmobilebcp' 'dbespmobilecanalm' 'dbradeeo' 'dbespmobilecashplus' 'dbespnord' 'dbradees' 'dbradeema' 'dbsync' 'dbtrx'
do
      curl -s -X POST -H 'Content-Type: application/json' --data "{
  \"name\": \"SQLServer ${db}\",
  \"config\": {
    \"connector.class\": \"io.debezium.connector.sqlserver.SqlServerConnector\",
    \"key.converter\": \"org.apache.kafka.connect.json.JsonConverter\",
    \"key.converter.schemas.enable\": \"false\",
    \"value.converter\": \"org.apache.kafka.connect.json.JsonConverter\",
    \"value.converter.schemas.enable\": \"false\",
    \"errors.log.enable\": true,
    \"errors.log.include.messages\": true,
    \"database.server.name\": \"${db}\",
    \"database.dbname\": \"${db}\",
    \"database.hostname\": \"${dbhostname}\",
    \"database.port\": \"${dbport}\",
    \"database.user\": \"${username}\",
    \"database.password\": \"${dbpassword}\",
    \"database.history.kafka.bootstrap.servers\": \"${kafkaserver}\",
    \"database.history.kafka.topic\": \"wallet.schema.history\"
    \"slot.name\" : \"${db}\"
  }
}" http://localhost:8083/connectors

done

