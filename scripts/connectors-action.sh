#!/bin/bash

hostname=${1:-'localhost'}
port=${2:-'8083'}
action=${3:-'pause'} # action = pause | resume

connectors=($( curl http://localhost:8083/connectors | sed "s/ /%20/g"  | jq -r '.[]' ))

for connector in "${connectors[@]}"
do
       curl -X PUT "http://localhost:8083/connectors/${connector}/${action}"
done


