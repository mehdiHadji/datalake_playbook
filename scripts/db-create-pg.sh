#!/bin/bash
set -x
dbname=$1
location=${2:-'/home/.db'}

if [ ! -d "$location/$dbname" ]; then

  psql -c "DROP DATABASE IF EXISTS ${dbname} ";
  psql -c "DROP TABLESPACE IF EXISTS ${dbname}";

  mkdir -p $location/$dbname

  psql -c "CREATE TABLESPACE ${dbname} OWNER postgres LOCATION '$location/${dbname}';"
  psql -c "CREATE DATABASE ${dbname} OWNER postgres TABLESPACE ${dbname} ENCODING 'UTF8';"

fi

